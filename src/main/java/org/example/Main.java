package org.example;

public class Main {
    public static void main(String[] args) {
        Client client = new MyClient();
        Handler handler = new MyHandler(client);
        
        ApplicationStatusResponse response = handler.performOperation("123456789");
        if (response instanceof ApplicationStatusResponse.Success) {
            ApplicationStatusResponse.Success successResponse = (ApplicationStatusResponse.Success) response;
            System.out.println("Success! Application ID: " + successResponse.id() + ", Status: " + successResponse.status());
        } else if (response instanceof ApplicationStatusResponse.Failure) {
            ApplicationStatusResponse.Failure failureResponse = (ApplicationStatusResponse.Failure) response;
            System.out.println("Failed after " + failureResponse.lastRequestTime() + ", Retries: " + failureResponse.retriesCount());
        }
    }
}

package org.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.time.Duration;
import java.util.Random;

public class MyClient implements Client {
    private static final Logger logger = LoggerFactory.getLogger(MyClient.class);
    private final Random random = new Random();

    @Override
    public Response getApplicationStatus1(String id) {
        int randomValue = random.nextInt(10);
        logResponse("getApplicationStatus1", randomValue);
        if (randomValue < 5) {
            return new Response.Success("Approved", id);
        } else if (randomValue < 8) {
            return new Response.RetryAfter(Duration.ofSeconds(random.nextInt(10)));
        } else {
            return new Response.Failure(new RuntimeException("Service 1 error"));
        }
    }

    @Override
    public Response getApplicationStatus2(String id) {
        int randomValue = random.nextInt(10);
        logResponse("getApplicationStatus2", randomValue);
        if (randomValue < 5) {
            return new Response.Success("Approved", id);
        } else if (randomValue < 8) {
            return new Response.RetryAfter(Duration.ofSeconds(random.nextInt(10)));
        } else {
            return new Response.Failure(new RuntimeException("Service 2 error"));
        }
    }

    private void logResponse(String methodName, int randomValue) {
        logger.info("{}: Received random value {}", methodName, randomValue);
    }
}

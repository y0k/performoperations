package org.example;

import java.time.Duration;

public class MyHandler implements Handler {
    private final Client client;

    public MyHandler(Client client) {
        this.client = client;
    }

    @Override
    public ApplicationStatusResponse performOperation(String id) {
        long startTime = System.currentTimeMillis();
        int retriesCount = 0;
        while (System.currentTimeMillis() - startTime < 15000) {
            Response response1 = client.getApplicationStatus1(id);
            if (response1 instanceof Response.Success) {
                Response.Success success = (Response.Success) response1;
                return new ApplicationStatusResponse.Success(success.applicationId(), success.applicationStatus());
            } else if (response1 instanceof Response.RetryAfter) {
                Response.RetryAfter retryAfter = (Response.RetryAfter) response1;
                try {
                    Thread.sleep(retryAfter.delay().toMillis());
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
                retriesCount++;
                continue;
            }

            Response response2 = client.getApplicationStatus2(id);
            if (response2 instanceof Response.Success) {
                Response.Success success = (Response.Success) response2;
                return new ApplicationStatusResponse.Success(success.applicationId(), success.applicationStatus());
            } else if (response2 instanceof Response.RetryAfter) {
                Response.RetryAfter retryAfter = (Response.RetryAfter) response2;
                try {
                    Thread.sleep(retryAfter.delay().toMillis());
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
                retriesCount++;
                continue;
            }

            retriesCount++;
        }

        return new ApplicationStatusResponse.Failure(Duration.ofMillis(System.currentTimeMillis() - startTime), retriesCount);
    }
}
